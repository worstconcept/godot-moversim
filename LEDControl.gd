@tool
extends Control
class_name LEDControl

@export var state: bool:
	set(val):
		state = val
		queue_redraw()

@export var onColor := Color.CHARTREUSE
@export var offColor := Color.DARK_OLIVE_GREEN

func _draw():
	var radius: float = size.length() / 3.0
	if state:
		draw_circle(size/2.0, radius, offColor)
		draw_circle(size/2.0, radius-1, onColor)
	else:
		draw_circle(size/2.0, radius, Color.BLACK)
		draw_circle(size/2.0, radius-1, offColor)

func _get_minimum_size()->Vector2:
	return Vector2(8,8)
