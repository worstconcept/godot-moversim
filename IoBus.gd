extends Node
class_name IoBus
#singleton_name SBus

var nRST: bool
var CLK: bool
var nWR: bool
var nRD: bool
var nS0: bool
var D: int
var A: int
var iPC: int
var iD: int
var iS: int

signal ticked
signal changed
