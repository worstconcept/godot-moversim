@tool
extends PanelContainer

@export var address: int = 0x0000
@export var ramSize := 0x0100
@export var ramInit := PackedInt32Array([0x0006,0x00ff,0x0007,0x00ff,0x0005,0x0000,0xDEAD,0xBEEF])
@export var addressEditable: bool = false:
	set(_val):
		addressEditable=false
		if Engine.is_editor_hint(): self._ready()

var memory := PackedInt32Array()

const hex1a := "___%01X"
const hex3b := "%03X_"
const hex4 := "%04X"
const wordname := "word_%04x"

func _ready()->void:
	if addressEditable:
		print("UNSUPPORTED!")
	%Address.text = hex4 % address
	%Address.editable = addressEditable
	%Size.text = hex4 % ramSize
	memory.resize(ramSize)
	memory.fill(0x0000)
	if ramInit:
		for i in range(0,ramInit.size()):
			memory.set(i,ramInit[i])
	for r in %Rows.get_children():
		r.queue_free()
	%Rows.add_child(Control.new())
	for i in range(0,16):
		var a := ( address + i ) % 16
		var label : Label = %Template/Label.duplicate()
		label.text = hex1a % a
		%Rows.add_child(label)
	for i in range(0,memory.size(),16):
		var label = %Template/Label.duplicate()
		label.text = hex3b % ((address+i)/16)
		%Rows.add_child(label)
		for j in range(i,i+16):
			if j >= memory.size(): break
			var input : LineEdit = %Template/Input.duplicate()
			input.name = wordname % j
			if ( address + j ) % 2 == 0:
				input.alignment = HORIZONTAL_ALIGNMENT_RIGHT
			else:
				input.alignment = HORIZONTAL_ALIGNMENT_LEFT
			input.text = hex4 % memory[j]
			var handler = func()->void:
				var new_text : String = input.text
				if new_text.length()==4 and new_text.is_valid_hex_number(false):
					memory.set(j,new_text.hex_to_int())
					input.text = new_text.to_upper()
				else:
					input.text = hex4 % memory[j]
				input.release_focus()
			input.text_submitted.connect(handler.unbind(1))
			input.focus_exited.connect(handler)
			input.gui_input.connect(func(ev: InputEvent)->void:
				var n : LineEdit = null
				if ev.is_action_pressed("ui_left"):
					n = get_node(input.focus_neighbor_left)
				if ev.is_action_pressed("ui_right"):
					n = get_node(input.focus_neighbor_right)
				if ev.is_action_pressed("ui_up"):
					n = get_node(input.focus_neighbor_top)
				if ev.is_action_pressed("ui_down"):
					n = get_node(input.focus_neighbor_bottom)
				if n:
					n.grab_focus.call_deferred()
			)
			%Rows.add_child(input)
	for i in range(0,memory.size()):
		var input : LineEdit = %Rows.find_child(wordname % i,false,false)
		if not input: continue
		var cleft : LineEdit = %Rows.find_child(wordname % (i-1),false,false)
		if cleft:
			var np := cleft.get_path()
			input.focus_neighbor_left = np
			input.focus_previous = np
		var cright : LineEdit = %Rows.find_child(wordname % (i+1),false,false)
		if cright:
			var np := cright.get_path()
			input.focus_neighbor_right = np
			input.focus_next = np
		var ctop : LineEdit = %Rows.find_child(wordname % (i-16),false,false)
		if ctop:
			var np := ctop.get_path()
			input.focus_neighbor_top = np
		var cbottom : LineEdit = %Rows.find_child(wordname % (i+16),false,false)
		if cbottom:
			var np := cbottom.get_path()
			input.focus_neighbor_bottom = np
		
	if Engine.is_editor_hint(): return
	SBus.ticked.connect(_on_ticked)

var highlighted : LineEdit = null
var highaddr: int = 0
var highS0: LineEdit = null
var highS0a: int = 0
func _on_ticked()->void:
	if highlighted && ((SBus.A != highaddr) || !SBus.nRST):
		if highlighted != highS0:
			highlighted.self_modulate = Color.WHITE
		highlighted = null
	if highS0 && ((SBus.A != highS0a && !SBus.nS0) || !SBus.nRST):
		if highlighted != highS0:
			highS0.self_modulate = Color.WHITE
		highS0 = null
	if address <= SBus.A && SBus.A-address < ramSize:
		var a := SBus.A-address
		var t : LineEdit = %Rows.find_child(wordname % a, false, false)
		if !SBus.nRD:
			SBus.D = memory[a]
			if !SBus.nS0:
				t.self_modulate = Color("7fff55")
				highS0 = t
				highS0a = SBus.A
			else:
				t.self_modulate = Color("557fff")
				highlighted = t
				highaddr = SBus.A
			SBus.changed.emit()
		elif !SBus.nWR:
			var d := SBus.D
			memory.set(a,d)
			t.self_modulate = Color("ffbf55")
			highlighted = t
			highaddr = SBus.A
			t.text = hex4 % d
