@tool
extends PanelContainer

@export var address: int = 0x0000:
	set(v):
		address = v
		if Engine.is_editor_hint(): self._ready()
@export var label: String = "A":
	set(v):
		label = v
		if Engine.is_editor_hint(): self._ready()

## func(data:int)->void
var out: Callable
## func()->data:int
var inp: Callable

const hex4 := "%04X"
func _ready()->void:
	%Address.text = hex4 % address
	%Portname.text = label
	if Engine.is_editor_hint(): return
	SBus.ticked.connect(_on_ticked)

var wait_for_desel := false
func _on_ticked()->void:
	if address == SBus.A and not wait_for_desel:
		if !SBus.nRD:
			wait_for_desel = true
			%state.text = "RD"
			var d := inp.call()
			%lastIn.text = hex4 % d
			SBus.D = d
			SBus.changed.emit()
		elif !SBus.nWR:
			wait_for_desel = true
			%state.text = "WR"
			var d := SBus.D
			%lastOut.text = hex4 % d
			out.call(d)
	elif address != SBus.A:
		wait_for_desel = false
		%state.text = "--"
