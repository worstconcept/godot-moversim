@tool
extends PanelContainer

@export var address: int = 0x0000:
	set(v):
		address = v
		if Engine.is_editor_hint(): self._ready()

var oA: int = 0
var oB: int = 0

const hex4 := "%04X"
func _ready()->void:
	%AA.text = hex4 % address
	%AB.text = hex4 % (address+1)
	%plus.text = hex4 % address
	%le.text = hex4 % (address+1)
	%nor.text = hex4 % (address+2)
	%ror.text = hex4 % (address+3)
	if Engine.is_editor_hint(): return
	SBus.ticked.connect(_on_ticked)

var highlighted : LineEdit = null
func _on_ticked()->void:
	if highlighted:
		highlighted.self_modulate = Color.WHITE
		highlighted = null
	if SBus.A == address:
		if !SBus.nRD:
			SBus.D = oA+oB
			SBus.changed.emit()
			highlighted = %plus
		elif !SBus.nWR:
			oA = SBus.D
			%OpA.text = hex4 % oA
			highlighted = %AA
	elif SBus.A == address+1:
		if !SBus.nRD:
			SBus.D = oA <= oB
			SBus.changed.emit()
			highlighted = %le
		elif !SBus.nWR:
			oB = SBus.D
			%OpB.text = hex4 % oB
			highlighted = %AB
	elif SBus.A == address+2:
		if !SBus.nRD:
			SBus.D = ~(oA|oB)
			SBus.changed.emit()
			highlighted = %nor
	elif SBus.A == address+3:
		if !SBus.nRD:
			SBus.D = ( oA >> (oB%16) ) | ( oA << (16 - (oB%16)) )
			SBus.changed.emit()
			highlighted = %ror
	if highlighted:
		highlighted.self_modulate = Color.GREEN_YELLOW
