extends Node2D

@export var clockrate: float = 6.0

var textTween: Tween
var clocktally: float = 0.0

func _ready():
	SBus.ticked.connect(_on_ticked)

func _physics_process(delta:float)->void:
	clocktally += delta
func _on_ticked()->void:
	var invrate := 0.5 / clockrate # seconds-per-halfclock
	if clocktally >= invrate:
		SBus.CLK = !SBus.CLK # do halfclock
		clocktally -= invrate
		SBus.changed.emit()

func text(str:String,time:=5.0)->Tween:
	%Message.visible_ratio=0
	%Message.text = str
	if textTween:
		textTween.kill()
	textTween = get_tree().create_tween()
	textTween.tween_property(%Message,'visible_ratio',1,time)
	return textTween

func _on_back_button_pressed():
		get_tree().change_scene_to_file.call_deferred("res://Main.tscn")
