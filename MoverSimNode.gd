extends Node

@onready var sim := MoverSim.new()

## how often to tick per second
# approximately, this is still synced to the physics thread.
@export var tickrate: float = 24.0

func tick()->void:
	sim.nRST=SBus.nRST
	sim.CLK=SBus.CLK
	if !SBus.nRD:
		sim.D=SBus.D
	sim.tick()
	SBus.nWR=sim.nWR
	SBus.nRD=sim.nRD
	SBus.nS0=sim.nS0
	SBus.iPC=sim.iPC
	SBus.iD=sim.iD
	SBus.iS=sim.iS
	if !SBus.nWR:
		SBus.D=sim.D
	SBus.A=sim.A
	SBus.ticked.emit()
	SBus.changed.emit()

var ticktally: float = 0.0
func _physics_process(delta:float)->void:
	var invrate := 1.0 / tickrate # seconds-per-tick
	ticktally += delta
	while ticktally >= invrate:
		tick()
		ticktally -= invrate
