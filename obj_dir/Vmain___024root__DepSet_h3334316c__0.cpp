// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vmain.h for the primary calling header

#include "verilated.h"

#include "Vmain___024root.h"

VL_INLINE_OPT void Vmain___024root___combo__TOP__0(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___combo__TOP__0\n"); );
    // Body
    vlSelf->Mover__DOT__core__DOT__addr_in = ((IData)(vlSelf->nRST)
                                               ? (IData)(vlSelf->Di)
                                               : 0U);
    vlSelf->Mover__DOT__core__DOT__step_clk = (1U & 
                                               (~ (IData)(vlSelf->CLK)));
}

VL_INLINE_OPT void Vmain___024root___sequent__TOP__0(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___sequent__TOP__0\n"); );
    // Body
    vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT___counter__DOT__val 
        = ((IData)(vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load)
            ? 0U : (7U & ((IData)(1U) + (IData)(vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT___counter__DOT__val))));
}

VL_INLINE_OPT void Vmain___024root___combo__TOP__1(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___combo__TOP__1\n"); );
    // Init
    CData/*0:0*/ Mover__DOT__core__DOT__step_ptr_addr_to_data;
    CData/*0:0*/ Mover__DOT__core__DOT__data_le;
    CData/*0:0*/ Mover__DOT__core__DOT__addr_le;
    CData/*5:0*/ Mover__DOT__core__DOT__step__DOT__step_bit;
    CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT__O__en0;
    // Body
    vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load 
        = (1U & ((~ (IData)(vlSelf->nRST)) | (6U == (IData)(vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT___counter__DOT__val))));
    Mover__DOT__core__DOT__step__DOT__step_bit = ((IData)(vlSelf->nRST)
                                                   ? 
                                                  (0x3fU 
                                                   & ((IData)(1U) 
                                                      << (IData)(vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT___counter__DOT__val)))
                                                   : 0U);
    vlSelf->nS0 = (1U & (~ ((IData)(vlSelf->nRST) & (IData)(Mover__DOT__core__DOT__step__DOT__step_bit))));
    vlSelf->Mover__DOT__core__DOT__step_inc_pc = ((IData)(vlSelf->nRST) 
                                                  & (IData)(
                                                            (0U 
                                                             != 
                                                             (0x12U 
                                                              & (IData)(Mover__DOT__core__DOT__step__DOT__step_bit)))));
    vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr 
        = ((IData)(vlSelf->nRST) & ((IData)(Mover__DOT__core__DOT__step__DOT__step_bit) 
                                    >> 5U));
    Mover__DOT__core__DOT__step_ptr_addr_to_data = 
        ((IData)(vlSelf->nRST) & ((IData)(Mover__DOT__core__DOT__step__DOT__step_bit) 
                                  >> 2U));
    vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr 
        = ((IData)(vlSelf->nRST) & ((IData)(Mover__DOT__core__DOT__step__DOT__step_bit) 
                                    | ((IData)(Mover__DOT__core__DOT__step__DOT__step_bit) 
                                       >> 3U)));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0 
        = (0xffU & (((((((((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                           << 7U) | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                     << 6U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                << 5U)) 
                        | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                           << 4U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                      << 3U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                 << 2U)) 
                     | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                        << 1U)) | (IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr)));
    Mover__DOT__core__DOT__data_le = ((IData)(vlSelf->CLK) 
                                      & ((~ (IData)(vlSelf->nRST)) 
                                         | (IData)(Mover__DOT__core__DOT__step_ptr_addr_to_data)));
    vlSelf->Mover__DOT__core__DOT__addr_oe = ((IData)(Mover__DOT__core__DOT__step_ptr_addr_to_data) 
                                              | (IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr));
    vlSelf->nRD = (1U & (~ ((IData)(Mover__DOT__core__DOT__step_ptr_addr_to_data) 
                            | (IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr))));
    Mover__DOT__core__DOT__addr_le = ((IData)(vlSelf->CLK) 
                                      & ((~ (IData)(vlSelf->nRST)) 
                                         | (IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr)));
    vlSelf->Mover__DOT__core__DOT__O__en0 = (0xffffU 
                                             & (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0) 
                                                 << 8U) 
                                                | (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0)));
    if (Mover__DOT__core__DOT__data_le) {
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xfU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xeU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xdU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xcU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xbU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xaU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 9U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 8U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 7U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 6U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 5U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 4U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 3U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 2U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 1U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val 
            = (1U & (IData)(vlSelf->Mover__DOT__core__DOT__addr_in));
    }
    Mover__DOT__core__DOT__r_addr__DOT__O__en0 = (0xffU 
                                                  & (((((((((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                            << 7U) 
                                                           | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                              << 6U)) 
                                                          | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                             << 5U)) 
                                                         | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                            << 4U)) 
                                                        | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                           << 3U)) 
                                                       | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                          << 2U)) 
                                                      | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                         << 1U)) 
                                                     | (IData)(vlSelf->Mover__DOT__core__DOT__addr_oe)));
    if (Mover__DOT__core__DOT__addr_le) {
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xfU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xeU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xdU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xcU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xbU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xaU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 9U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 8U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 7U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 6U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 5U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 4U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 3U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 2U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 1U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val 
            = (1U & (IData)(vlSelf->Mover__DOT__core__DOT__addr_in));
    }
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out8 
        = ((0x7fU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out8)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val)) 
              << 7U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out9 
        = ((0xbfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out9)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val)) 
              << 6U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out10 
        = ((0xdfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out10)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val)) 
              << 5U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out11 
        = ((0xefU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out11)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val)) 
              << 4U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out12 
        = ((0xf7U & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out12)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val)) 
              << 3U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out13 
        = ((0xfbU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out13)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val)) 
              << 2U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out14 
        = ((0xfdU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out14)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val)) 
              << 1U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out15 
        = ((0xfeU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out15)) 
           | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
              & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val)));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out8 
        = ((0x7fU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out8)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val)) 
              << 7U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out9 
        = ((0xbfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out9)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val)) 
              << 6U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out10 
        = ((0xdfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out10)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val)) 
              << 5U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out11 
        = ((0xefU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out11)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val)) 
              << 4U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out12 
        = ((0xf7U & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out12)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val)) 
              << 3U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out13 
        = ((0xfbU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out13)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val)) 
              << 2U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out14 
        = ((0xfdU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out14)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val)) 
              << 1U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out15 
        = ((0xfeU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out15)) 
           | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
              & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val)));
    vlSelf->Mover__DOT__core__DOT__O__en1 = (0xffffU 
                                             & (((IData)(Mover__DOT__core__DOT__r_addr__DOT__O__en0) 
                                                 << 8U) 
                                                | (IData)(Mover__DOT__core__DOT__r_addr__DOT__O__en0)));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out8 
        = ((0x7fU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out8)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val)) 
              << 7U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out9 
        = ((0xbfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out9)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val)) 
              << 6U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out10 
        = ((0xdfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out10)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val)) 
              << 5U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out11 
        = ((0xefU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out11)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val)) 
              << 4U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out12 
        = ((0xf7U & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out12)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val)) 
              << 3U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out13 
        = ((0xfbU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out13)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val)) 
              << 2U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out14 
        = ((0xfdU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out14)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val)) 
              << 1U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out15 
        = ((0xfeU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out15)) 
           | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
              & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val)));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out8 
        = ((0x7fU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out8)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val)) 
              << 7U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out9 
        = ((0xbfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out9)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val)) 
              << 6U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out10 
        = ((0xdfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out10)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val)) 
              << 5U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out11 
        = ((0xefU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out11)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val)) 
              << 4U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out12 
        = ((0xf7U & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out12)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val)) 
              << 3U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out13 
        = ((0xfbU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out13)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val)) 
              << 2U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out14 
        = ((0xfdU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out14)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val)) 
              << 1U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out15 
        = ((0xfeU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out15)) 
           | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
              & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val)));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out2 
        = ((0xffU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out2)) 
           | ((((((((((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out8) 
                      << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                << 0xfU)) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out9) 
                                              << 8U) 
                                             & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                << 0xeU))) 
                   | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out10) 
                       << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                 << 0xdU))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out11) 
                                                << 8U) 
                                               & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                  << 0xcU))) 
                 | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out12) 
                     << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                               << 0xbU))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out13) 
                                              << 8U) 
                                             & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                << 0xaU))) 
               | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out14) 
                   << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                             << 9U))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out15) 
                                          & (IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr)) 
                                         << 8U)));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out3 
        = ((0xff00U & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out3)) 
           | (((((((((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out8) 
                     & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                        << 7U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out9) 
                                   & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                      << 6U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out10) 
                                                  & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                     << 5U))) 
                  | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out11) 
                     & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                        << 4U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out12) 
                                    & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                       << 3U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out13) 
                                                   & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                      << 2U))) 
               | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out14) 
                  & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                     << 1U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out15) 
                                 & (IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr))));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out2 
        = ((0xffU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out2)) 
           | ((((((((((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out8) 
                      << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                << 0xfU)) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out9) 
                                              << 8U) 
                                             & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                << 0xeU))) 
                   | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out10) 
                       << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                 << 0xdU))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out11) 
                                                << 8U) 
                                               & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                  << 0xcU))) 
                 | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out12) 
                     << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                               << 0xbU))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out13) 
                                              << 8U) 
                                             & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                << 0xaU))) 
               | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out14) 
                   << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                             << 9U))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out15) 
                                          & (IData)(vlSelf->Mover__DOT__core__DOT__addr_oe)) 
                                         << 8U)));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out3 
        = ((0xff00U & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out3)) 
           | (((((((((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out8) 
                     & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                        << 7U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out9) 
                                   & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                      << 6U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out10) 
                                                  & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                     << 5U))) 
                  | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out11) 
                     & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                        << 4U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out12) 
                                    & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                       << 3U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out13) 
                                                   & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                      << 2U))) 
               | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out14) 
                  & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                     << 1U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out15) 
                                 & (IData)(vlSelf->Mover__DOT__core__DOT__addr_oe))));
    vlSelf->Mover__DOT__core__DOT__addr_out = ((((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out2) 
                                                 & ((IData)(Mover__DOT__core__DOT__r_addr__DOT__O__en0) 
                                                    << 8U)) 
                                                | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out3) 
                                                   & (IData)(Mover__DOT__core__DOT__r_addr__DOT__O__en0))) 
                                               & (IData)(vlSelf->Mover__DOT__core__DOT__O__en1));
}

VL_INLINE_OPT void Vmain___024root___sequent__TOP__1(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___sequent__TOP__1\n"); );
    // Body
    vlSelf->Mover__DOT__core__DOT__pc__DOT__val = (0xffffU 
                                                   & ((IData)(vlSelf->Mover__DOT__core__DOT__pc_le)
                                                       ? 
                                                      ((IData)(vlSelf->nRST)
                                                        ? 
                                                       ((IData)(vlSelf->Mover__DOT__core__DOT__jump)
                                                         ? (IData)(vlSelf->Mover__DOT__core__DOT__data_out)
                                                         : 0U)
                                                        : 0U)
                                                       : 
                                                      ((IData)(1U) 
                                                       + (IData)(vlSelf->Mover__DOT__core__DOT__pc__DOT__val))));
}

VL_INLINE_OPT void Vmain___024root___combo__TOP__2(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___combo__TOP__2\n"); );
    // Init
    CData/*0:0*/ Mover__DOT___wr;
    SData/*15:0*/ Mover__DOT__core__DOT__bus_data_out__out__en2;
    SData/*15:0*/ Mover__DOT__core__DOT__bus_addr__out__en3;
    // Body
    vlSelf->Mover__DOT__core__DOT__data_out = ((((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out2) 
                                                 & ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0) 
                                                    << 8U)) 
                                                | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out3) 
                                                   & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0))) 
                                               & (IData)(vlSelf->Mover__DOT__core__DOT__O__en0));
    vlSelf->Mover__DOT__core__DOT__jump = ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                           & (0U == (IData)(vlSelf->Mover__DOT__core__DOT__addr_out)));
    vlSelf->Mover__DOT__core__DOT__pc_le = ((IData)(vlSelf->CLK) 
                                            & ((~ (IData)(vlSelf->nRST)) 
                                               | (IData)(vlSelf->Mover__DOT__core__DOT__jump)));
    Mover__DOT__core__DOT__bus_addr__out__en3 = ((IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr)
                                                  ? 0xffffU
                                                  : 
                                                 (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                   & (~ (IData)(vlSelf->Mover__DOT__core__DOT__jump)))
                                                   ? (IData)(vlSelf->Mover__DOT__core__DOT__O__en1)
                                                   : 0U));
    Mover__DOT___wr = ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                       & (~ (IData)(vlSelf->Mover__DOT__core__DOT__jump)));
    vlSelf->A = (((((IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr)
                     ? ((IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr)
                         ? (IData)(vlSelf->Mover__DOT__core__DOT__pc__DOT__val)
                         : 0U) : (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                   & (~ (IData)(vlSelf->Mover__DOT__core__DOT__jump)))
                                   ? (IData)(vlSelf->Mover__DOT__core__DOT__addr_out)
                                   : 0U)) & (IData)(Mover__DOT__core__DOT__bus_addr__out__en3)) 
                  & (IData)(Mover__DOT__core__DOT__bus_addr__out__en3)) 
                 & (IData)(Mover__DOT__core__DOT__bus_addr__out__en3));
    vlSelf->nWR = (1U & (~ (IData)(Mover__DOT___wr)));
    Mover__DOT__core__DOT__bus_data_out__out__en2 = 
        ((IData)(Mover__DOT___wr) ? (IData)(vlSelf->Mover__DOT__core__DOT__O__en0)
          : 0U);
    vlSelf->Do = (((((IData)(Mover__DOT___wr) ? (IData)(vlSelf->Mover__DOT__core__DOT__data_out)
                      : 0U) & (IData)(Mover__DOT__core__DOT__bus_data_out__out__en2)) 
                   & (IData)(Mover__DOT__core__DOT__bus_data_out__out__en2)) 
                  & (IData)(Mover__DOT__core__DOT__bus_data_out__out__en2));
}

void Vmain___024root___eval(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___eval\n"); );
    // Body
    Vmain___024root___combo__TOP__0(vlSelf);
    if ((((IData)(vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load) 
          & (~ (IData)(vlSelf->__Vclklast__TOP____VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load))) 
         | ((IData)(vlSelf->Mover__DOT__core__DOT__step_clk) 
            & (~ (IData)(vlSelf->__Vclklast__TOP__Mover__DOT__core__DOT__step_clk))))) {
        Vmain___024root___sequent__TOP__0(vlSelf);
    }
    Vmain___024root___combo__TOP__1(vlSelf);
    if ((((IData)(vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__pc_le) 
          & (~ (IData)(vlSelf->__Vclklast__TOP____VinpClk__TOP__Mover__DOT__core__DOT__pc_le))) 
         | ((IData)(vlSelf->Mover__DOT__core__DOT__step_inc_pc) 
            & (~ (IData)(vlSelf->__Vclklast__TOP__Mover__DOT__core__DOT__step_inc_pc))))) {
        Vmain___024root___sequent__TOP__1(vlSelf);
    }
    Vmain___024root___combo__TOP__2(vlSelf);
    // Final
    vlSelf->__Vclklast__TOP____VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load 
        = vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load;
    vlSelf->__Vclklast__TOP__Mover__DOT__core__DOT__step_clk 
        = vlSelf->Mover__DOT__core__DOT__step_clk;
    vlSelf->__Vclklast__TOP____VinpClk__TOP__Mover__DOT__core__DOT__pc_le 
        = vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__pc_le;
    vlSelf->__Vclklast__TOP__Mover__DOT__core__DOT__step_inc_pc 
        = vlSelf->Mover__DOT__core__DOT__step_inc_pc;
    vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load 
        = vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load;
    vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__pc_le 
        = vlSelf->Mover__DOT__core__DOT__pc_le;
}

QData Vmain___024root___change_request_1(Vmain___024root* vlSelf);

VL_INLINE_OPT QData Vmain___024root___change_request(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___change_request\n"); );
    // Body
    return (Vmain___024root___change_request_1(vlSelf));
}

VL_INLINE_OPT QData Vmain___024root___change_request_1(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___change_request_1\n"); );
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    __req |= ((vlSelf->Mover__DOT__core__DOT__pc_le ^ vlSelf->__Vchglast__TOP__Mover__DOT__core__DOT__pc_le)
         | (vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load ^ vlSelf->__Vchglast__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load));
    VL_DEBUG_IF( if(__req && ((vlSelf->Mover__DOT__core__DOT__pc_le ^ vlSelf->__Vchglast__TOP__Mover__DOT__core__DOT__pc_le))) VL_DBG_MSGF("        CHANGE: vinclude/core.sv:16: Mover.core.pc_le\n"); );
    VL_DEBUG_IF( if(__req && ((vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load ^ vlSelf->__Vchglast__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load))) VL_DBG_MSGF("        CHANGE: vinclude/counter_onehot.sv:12: Mover.core.step.step_counter_decoder.count_load\n"); );
    // Final
    vlSelf->__Vchglast__TOP__Mover__DOT__core__DOT__pc_le 
        = vlSelf->Mover__DOT__core__DOT__pc_le;
    vlSelf->__Vchglast__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load 
        = vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load;
    return __req;
}

#ifdef VL_DEBUG
void Vmain___024root___eval_debug_assertions(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((vlSelf->nRST & 0xfeU))) {
        Verilated::overWidthError("nRST");}
    if (VL_UNLIKELY((vlSelf->CLK & 0xfeU))) {
        Verilated::overWidthError("CLK");}
}
#endif  // VL_DEBUG
