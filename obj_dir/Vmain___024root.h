// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vmain.h for the primary calling header

#ifndef VERILATED_VMAIN___024ROOT_H_
#define VERILATED_VMAIN___024ROOT_H_  // guard

#include "verilated.h"

class Vmain__Syms;
VL_MODULE(Vmain___024root) {
  public:

    // DESIGN SPECIFIC STATE
    // Anonymous structures to workaround compiler member-count bugs
    struct {
        CData/*0:0*/ Mover__DOT__core__DOT__step_clk;
        CData/*0:0*/ Mover__DOT__core__DOT__step_inc_pc;
        CData/*0:0*/ Mover__DOT__core__DOT__pc_le;
        CData/*0:0*/ Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load;
        VL_IN8(nRST,0,0);
        VL_IN8(CLK,0,0);
        VL_OUT8(nWR,0,0);
        VL_OUT8(nRD,0,0);
        VL_OUT8(nS0,0,0);
        CData/*0:0*/ Mover__DOT__core__DOT__step_ptr_pc_to_addr;
        CData/*0:0*/ Mover__DOT__core__DOT__step_data_to_ptr_addr;
        CData/*0:0*/ Mover__DOT__core__DOT__addr_oe;
        CData/*0:0*/ Mover__DOT__core__DOT__jump;
        CData/*2:0*/ Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT___counter__DOT__val;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT__O__en0;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out8;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out9;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out10;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out11;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out12;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out13;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out14;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out15;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out8;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out9;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out10;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out11;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out12;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out13;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out14;
        CData/*7:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out15;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out8;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out9;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out10;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out11;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out12;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out13;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out14;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out15;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out8;
    };
    struct {
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out9;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out10;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out11;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out12;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out13;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out14;
        CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out15;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val;
        CData/*0:0*/ Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val;
        CData/*0:0*/ __VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load;
        CData/*0:0*/ __VinpClk__TOP__Mover__DOT__core__DOT__pc_le;
        CData/*0:0*/ __Vclklast__TOP____VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load;
        CData/*0:0*/ __Vclklast__TOP__Mover__DOT__core__DOT__step_clk;
        CData/*0:0*/ __Vclklast__TOP____VinpClk__TOP__Mover__DOT__core__DOT__pc_le;
        CData/*0:0*/ __Vclklast__TOP__Mover__DOT__core__DOT__step_inc_pc;
        CData/*0:0*/ __Vchglast__TOP__Mover__DOT__core__DOT__pc_le;
        CData/*0:0*/ __Vchglast__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load;
        VL_IN16(Di,15,0);
        VL_OUT16(A,15,0);
        VL_OUT16(Do,15,0);
        SData/*15:0*/ Mover__DOT__core__DOT__data_out;
        SData/*15:0*/ Mover__DOT__core__DOT__addr_out;
        SData/*15:0*/ Mover__DOT__core__DOT__addr_in;
        SData/*15:0*/ Mover__DOT__core__DOT__O__en0;
        SData/*15:0*/ Mover__DOT__core__DOT__O__en1;
        SData/*15:0*/ Mover__DOT__core__DOT__pc__DOT__val;
        SData/*15:0*/ Mover__DOT__core__DOT__r_data__DOT__O__out__out2;
        SData/*15:0*/ Mover__DOT__core__DOT__r_data__DOT__O__out__out3;
        SData/*15:0*/ Mover__DOT__core__DOT__r_addr__DOT__O__out__out2;
        SData/*15:0*/ Mover__DOT__core__DOT__r_addr__DOT__O__out__out3;
    };

    // INTERNAL VARIABLES
    Vmain__Syms* const vlSymsp;

    // CONSTRUCTORS
    Vmain___024root(Vmain__Syms* symsp, const char* name);
    ~Vmain___024root();
    VL_UNCOPYABLE(Vmain___024root);

    // INTERNAL METHODS
    void __Vconfigure(bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
