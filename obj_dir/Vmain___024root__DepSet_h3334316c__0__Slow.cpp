// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vmain.h for the primary calling header

#include "verilated.h"

#include "Vmain___024root.h"

VL_ATTR_COLD void Vmain___024root___settle__TOP__0(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___settle__TOP__0\n"); );
    // Init
    CData/*0:0*/ Mover__DOT___wr;
    CData/*0:0*/ Mover__DOT__core__DOT__step_ptr_addr_to_data;
    CData/*0:0*/ Mover__DOT__core__DOT__data_le;
    CData/*0:0*/ Mover__DOT__core__DOT__addr_le;
    SData/*15:0*/ Mover__DOT__core__DOT__bus_data_out__out__en2;
    SData/*15:0*/ Mover__DOT__core__DOT__bus_addr__out__en3;
    CData/*5:0*/ Mover__DOT__core__DOT__step__DOT__step_bit;
    CData/*7:0*/ Mover__DOT__core__DOT__r_addr__DOT__O__en0;
    // Body
    vlSelf->Mover__DOT__core__DOT__addr_in = ((IData)(vlSelf->nRST)
                                               ? (IData)(vlSelf->Di)
                                               : 0U);
    vlSelf->Mover__DOT__core__DOT__step_clk = (1U & 
                                               (~ (IData)(vlSelf->CLK)));
    vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load 
        = (1U & ((~ (IData)(vlSelf->nRST)) | (6U == (IData)(vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT___counter__DOT__val))));
    Mover__DOT__core__DOT__step__DOT__step_bit = ((IData)(vlSelf->nRST)
                                                   ? 
                                                  (0x3fU 
                                                   & ((IData)(1U) 
                                                      << (IData)(vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT___counter__DOT__val)))
                                                   : 0U);
    vlSelf->nS0 = (1U & (~ ((IData)(vlSelf->nRST) & (IData)(Mover__DOT__core__DOT__step__DOT__step_bit))));
    vlSelf->Mover__DOT__core__DOT__step_inc_pc = ((IData)(vlSelf->nRST) 
                                                  & (IData)(
                                                            (0U 
                                                             != 
                                                             (0x12U 
                                                              & (IData)(Mover__DOT__core__DOT__step__DOT__step_bit)))));
    vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr 
        = ((IData)(vlSelf->nRST) & ((IData)(Mover__DOT__core__DOT__step__DOT__step_bit) 
                                    >> 5U));
    Mover__DOT__core__DOT__step_ptr_addr_to_data = 
        ((IData)(vlSelf->nRST) & ((IData)(Mover__DOT__core__DOT__step__DOT__step_bit) 
                                  >> 2U));
    vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr 
        = ((IData)(vlSelf->nRST) & ((IData)(Mover__DOT__core__DOT__step__DOT__step_bit) 
                                    | ((IData)(Mover__DOT__core__DOT__step__DOT__step_bit) 
                                       >> 3U)));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0 
        = (0xffU & (((((((((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                           << 7U) | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                     << 6U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                << 5U)) 
                        | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                           << 4U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                      << 3U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                 << 2U)) 
                     | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                        << 1U)) | (IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr)));
    Mover__DOT__core__DOT__data_le = ((IData)(vlSelf->CLK) 
                                      & ((~ (IData)(vlSelf->nRST)) 
                                         | (IData)(Mover__DOT__core__DOT__step_ptr_addr_to_data)));
    vlSelf->Mover__DOT__core__DOT__addr_oe = ((IData)(Mover__DOT__core__DOT__step_ptr_addr_to_data) 
                                              | (IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr));
    vlSelf->nRD = (1U & (~ ((IData)(Mover__DOT__core__DOT__step_ptr_addr_to_data) 
                            | (IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr))));
    Mover__DOT__core__DOT__addr_le = ((IData)(vlSelf->CLK) 
                                      & ((~ (IData)(vlSelf->nRST)) 
                                         | (IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr)));
    vlSelf->Mover__DOT__core__DOT__O__en0 = (0xffffU 
                                             & (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0) 
                                                 << 8U) 
                                                | (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0)));
    if (Mover__DOT__core__DOT__data_le) {
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xfU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xeU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xdU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xcU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xbU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xaU));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 9U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 8U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 7U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 6U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 5U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 4U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 3U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 2U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 1U));
        vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val 
            = (1U & (IData)(vlSelf->Mover__DOT__core__DOT__addr_in));
    }
    Mover__DOT__core__DOT__r_addr__DOT__O__en0 = (0xffU 
                                                  & (((((((((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                            << 7U) 
                                                           | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                              << 6U)) 
                                                          | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                             << 5U)) 
                                                         | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                            << 4U)) 
                                                        | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                           << 3U)) 
                                                       | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                          << 2U)) 
                                                      | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                         << 1U)) 
                                                     | (IData)(vlSelf->Mover__DOT__core__DOT__addr_oe)));
    if (Mover__DOT__core__DOT__addr_le) {
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xfU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xeU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xdU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xcU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xbU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 0xaU));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 9U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 8U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 7U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 6U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 5U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 4U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 3U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 2U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val 
            = (1U & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_in) 
                     >> 1U));
        vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val 
            = (1U & (IData)(vlSelf->Mover__DOT__core__DOT__addr_in));
    }
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out8 
        = ((0x7fU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out8)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val)) 
              << 7U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out9 
        = ((0xbfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out9)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val)) 
              << 6U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out10 
        = ((0xdfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out10)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val)) 
              << 5U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out11 
        = ((0xefU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out11)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val)) 
              << 4U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out12 
        = ((0xf7U & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out12)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val)) 
              << 3U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out13 
        = ((0xfbU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out13)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val)) 
              << 2U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out14 
        = ((0xfdU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out14)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val)) 
              << 1U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out15 
        = ((0xfeU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out15)) 
           | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
              & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val)));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out8 
        = ((0x7fU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out8)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val)) 
              << 7U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out9 
        = ((0xbfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out9)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val)) 
              << 6U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out10 
        = ((0xdfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out10)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val)) 
              << 5U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out11 
        = ((0xefU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out11)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val)) 
              << 4U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out12 
        = ((0xf7U & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out12)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val)) 
              << 3U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out13 
        = ((0xfbU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out13)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val)) 
              << 2U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out14 
        = ((0xfdU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out14)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val)) 
              << 1U));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out15 
        = ((0xfeU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out15)) 
           | ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
              & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val)));
    vlSelf->Mover__DOT__core__DOT__O__en1 = (0xffffU 
                                             & (((IData)(Mover__DOT__core__DOT__r_addr__DOT__O__en0) 
                                                 << 8U) 
                                                | (IData)(Mover__DOT__core__DOT__r_addr__DOT__O__en0)));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out8 
        = ((0x7fU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out8)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val)) 
              << 7U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out9 
        = ((0xbfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out9)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val)) 
              << 6U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out10 
        = ((0xdfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out10)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val)) 
              << 5U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out11 
        = ((0xefU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out11)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val)) 
              << 4U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out12 
        = ((0xf7U & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out12)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val)) 
              << 3U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out13 
        = ((0xfbU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out13)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val)) 
              << 2U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out14 
        = ((0xfdU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out14)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val)) 
              << 1U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out15 
        = ((0xfeU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out15)) 
           | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
              & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val)));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out8 
        = ((0x7fU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out8)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val)) 
              << 7U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out9 
        = ((0xbfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out9)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val)) 
              << 6U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out10 
        = ((0xdfU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out10)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val)) 
              << 5U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out11 
        = ((0xefU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out11)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val)) 
              << 4U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out12 
        = ((0xf7U & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out12)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val)) 
              << 3U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out13 
        = ((0xfbU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out13)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val)) 
              << 2U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out14 
        = ((0xfdU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out14)) 
           | (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
               & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val)) 
              << 1U));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out15 
        = ((0xfeU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out15)) 
           | ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
              & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val)));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out2 
        = ((0xffU & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out2)) 
           | ((((((((((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out8) 
                      << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                << 0xfU)) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out9) 
                                              << 8U) 
                                             & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                << 0xeU))) 
                   | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out10) 
                       << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                 << 0xdU))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out11) 
                                                << 8U) 
                                               & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                  << 0xcU))) 
                 | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out12) 
                     << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                               << 0xbU))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out13) 
                                              << 8U) 
                                             & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                << 0xaU))) 
               | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out14) 
                   << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                             << 9U))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out15) 
                                          & (IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr)) 
                                         << 8U)));
    vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out3 
        = ((0xff00U & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out3)) 
           | (((((((((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out8) 
                     & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                        << 7U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out9) 
                                   & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                      << 6U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out10) 
                                                  & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                     << 5U))) 
                  | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out11) 
                     & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                        << 4U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out12) 
                                    & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                       << 3U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out13) 
                                                   & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                                      << 2U))) 
               | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out14) 
                  & ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                     << 1U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out15) 
                                 & (IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr))));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out2 
        = ((0xffU & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out2)) 
           | ((((((((((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out8) 
                      << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                << 0xfU)) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out9) 
                                              << 8U) 
                                             & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                << 0xeU))) 
                   | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out10) 
                       << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                 << 0xdU))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out11) 
                                                << 8U) 
                                               & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                  << 0xcU))) 
                 | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out12) 
                     << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                               << 0xbU))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out13) 
                                              << 8U) 
                                             & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                << 0xaU))) 
               | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out14) 
                   << 8U) & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                             << 9U))) | (((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out15) 
                                          & (IData)(vlSelf->Mover__DOT__core__DOT__addr_oe)) 
                                         << 8U)));
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out3 
        = ((0xff00U & (IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out3)) 
           | (((((((((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out8) 
                     & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                        << 7U)) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out9) 
                                   & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                      << 6U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out10) 
                                                  & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                     << 5U))) 
                  | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out11) 
                     & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                        << 4U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out12) 
                                    & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                       << 3U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out13) 
                                                   & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                      << 2U))) 
               | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out14) 
                  & ((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                     << 1U))) | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out15) 
                                 & (IData)(vlSelf->Mover__DOT__core__DOT__addr_oe))));
    vlSelf->Mover__DOT__core__DOT__data_out = ((((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out2) 
                                                 & ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0) 
                                                    << 8U)) 
                                                | ((IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out3) 
                                                   & (IData)(vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0))) 
                                               & (IData)(vlSelf->Mover__DOT__core__DOT__O__en0));
    vlSelf->Mover__DOT__core__DOT__addr_out = ((((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out2) 
                                                 & ((IData)(Mover__DOT__core__DOT__r_addr__DOT__O__en0) 
                                                    << 8U)) 
                                                | ((IData)(vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out3) 
                                                   & (IData)(Mover__DOT__core__DOT__r_addr__DOT__O__en0))) 
                                               & (IData)(vlSelf->Mover__DOT__core__DOT__O__en1));
    vlSelf->Mover__DOT__core__DOT__jump = ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                                           & (0U == (IData)(vlSelf->Mover__DOT__core__DOT__addr_out)));
    vlSelf->Mover__DOT__core__DOT__pc_le = ((IData)(vlSelf->CLK) 
                                            & ((~ (IData)(vlSelf->nRST)) 
                                               | (IData)(vlSelf->Mover__DOT__core__DOT__jump)));
    Mover__DOT__core__DOT__bus_addr__out__en3 = ((IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr)
                                                  ? 0xffffU
                                                  : 
                                                 (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                                   & (~ (IData)(vlSelf->Mover__DOT__core__DOT__jump)))
                                                   ? (IData)(vlSelf->Mover__DOT__core__DOT__O__en1)
                                                   : 0U));
    Mover__DOT___wr = ((IData)(vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr) 
                       & (~ (IData)(vlSelf->Mover__DOT__core__DOT__jump)));
    vlSelf->A = (((((IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr)
                     ? ((IData)(vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr)
                         ? (IData)(vlSelf->Mover__DOT__core__DOT__pc__DOT__val)
                         : 0U) : (((IData)(vlSelf->Mover__DOT__core__DOT__addr_oe) 
                                   & (~ (IData)(vlSelf->Mover__DOT__core__DOT__jump)))
                                   ? (IData)(vlSelf->Mover__DOT__core__DOT__addr_out)
                                   : 0U)) & (IData)(Mover__DOT__core__DOT__bus_addr__out__en3)) 
                  & (IData)(Mover__DOT__core__DOT__bus_addr__out__en3)) 
                 & (IData)(Mover__DOT__core__DOT__bus_addr__out__en3));
    vlSelf->nWR = (1U & (~ (IData)(Mover__DOT___wr)));
    Mover__DOT__core__DOT__bus_data_out__out__en2 = 
        ((IData)(Mover__DOT___wr) ? (IData)(vlSelf->Mover__DOT__core__DOT__O__en0)
          : 0U);
    vlSelf->Do = (((((IData)(Mover__DOT___wr) ? (IData)(vlSelf->Mover__DOT__core__DOT__data_out)
                      : 0U) & (IData)(Mover__DOT__core__DOT__bus_data_out__out__en2)) 
                   & (IData)(Mover__DOT__core__DOT__bus_data_out__out__en2)) 
                  & (IData)(Mover__DOT__core__DOT__bus_data_out__out__en2));
}

VL_ATTR_COLD void Vmain___024root___eval_initial(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___eval_initial\n"); );
    // Body
    vlSelf->__Vclklast__TOP____VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load 
        = vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load;
    vlSelf->__Vclklast__TOP__Mover__DOT__core__DOT__step_clk 
        = vlSelf->Mover__DOT__core__DOT__step_clk;
    vlSelf->__Vclklast__TOP____VinpClk__TOP__Mover__DOT__core__DOT__pc_le 
        = vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__pc_le;
    vlSelf->__Vclklast__TOP__Mover__DOT__core__DOT__step_inc_pc 
        = vlSelf->Mover__DOT__core__DOT__step_inc_pc;
}

VL_ATTR_COLD void Vmain___024root___eval_settle(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___eval_settle\n"); );
    // Body
    Vmain___024root___settle__TOP__0(vlSelf);
}

VL_ATTR_COLD void Vmain___024root___final(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___final\n"); );
}

VL_ATTR_COLD void Vmain___024root___ctor_var_reset(Vmain___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmain__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmain___024root___ctor_var_reset\n"); );
    // Body
    vlSelf->nRST = VL_RAND_RESET_I(1);
    vlSelf->CLK = VL_RAND_RESET_I(1);
    vlSelf->Di = VL_RAND_RESET_I(16);
    vlSelf->nWR = VL_RAND_RESET_I(1);
    vlSelf->nRD = VL_RAND_RESET_I(1);
    vlSelf->nS0 = VL_RAND_RESET_I(1);
    vlSelf->A = VL_RAND_RESET_I(16);
    vlSelf->Do = VL_RAND_RESET_I(16);
    vlSelf->Mover__DOT__core__DOT__step_clk = 0;
    vlSelf->Mover__DOT__core__DOT__step_ptr_pc_to_addr = 0;
    vlSelf->Mover__DOT__core__DOT__step_inc_pc = 0;
    vlSelf->Mover__DOT__core__DOT__step_data_to_ptr_addr = 0;
    vlSelf->Mover__DOT__core__DOT__pc_le = 0;
    vlSelf->Mover__DOT__core__DOT__data_out = VL_RAND_RESET_I(16);
    vlSelf->Mover__DOT__core__DOT__addr_out = VL_RAND_RESET_I(16);
    vlSelf->Mover__DOT__core__DOT__addr_in = VL_RAND_RESET_I(16);
    vlSelf->Mover__DOT__core__DOT__addr_oe = 0;
    vlSelf->Mover__DOT__core__DOT__jump = 0;
    vlSelf->Mover__DOT__core__DOT__O__en0 = 0;
    vlSelf->Mover__DOT__core__DOT__O__en1 = 0;
    vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load = 0;
    vlSelf->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT___counter__DOT__val = VL_RAND_RESET_I(3);
    vlSelf->Mover__DOT__core__DOT__pc__DOT__val = VL_RAND_RESET_I(16);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__en0 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out2 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT__O__out__out3 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out8 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out9 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out10 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out11 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out12 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out13 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out14 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT__O__out__out15 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out8 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out9 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out10 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out11 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out12 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out13 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out14 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT__O__out__out15 = 0;
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out2 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT__O__out__out3 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out8 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out9 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out10 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out11 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out12 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out13 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out14 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT__O__out__out15 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out8 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out9 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out10 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out11 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out12 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out13 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out14 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT__O__out__out15 = 0;
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->Mover__DOT__core__DOT__r_addr__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val = VL_RAND_RESET_I(1);
    vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load = 0;
    vlSelf->__VinpClk__TOP__Mover__DOT__core__DOT__pc_le = 0;
    vlSelf->__Vchglast__TOP__Mover__DOT__core__DOT__pc_le = 0;
    vlSelf->__Vchglast__TOP__Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT__count_load = 0;
}
