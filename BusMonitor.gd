@tool
extends PanelContainer

@export var internals:= false:
	set(val):
		internals=val
		if Engine.is_editor_hint(): self._ready()

var wantRST := false

func _ready()->void:
	%Internals.visible=internals
	if Engine.is_editor_hint(): return
	SBus.changed.connect(_on_bus_changed)
	SBus.ticked.connect(_on_ticked)

func _on_bus_changed()->void:
	const hex4 := "%04X"
	const hex1 := "%01X"
	%CLK.state  =  SBus.CLK
	%nRST.state =! SBus.nRST
	%nS0.state  =! SBus.nS0
	%nRD.state  =! SBus.nRD
	%nWR.state  =! SBus.nWR
	%A.text   = hex4 % SBus.A
	%D.text   = hex4 % SBus.D
	if internals:
		%iPC.text = hex4 % SBus.iPC
		%iD.text  = hex4 % SBus.iD
		%iS.text  = hex1 % SBus.iS

var prevCLK := false
func _on_ticked()->void:
	if wantRST and SBus.CLK != prevCLK:
		SBus.nRST = !SBus.nRST
		wantRST = false
	prevCLK = SBus.CLK

func _on_RST_pressed():
	wantRST=true
