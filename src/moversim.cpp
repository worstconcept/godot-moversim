#include "moversim.h"

#include "../obj_dir/Vmain___024root.h"

#include <godot_cpp/classes/global_constants.hpp>
#include <godot_cpp/classes/label.hpp>
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/core/object.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <memory>

using namespace godot;

void MoverSim::_bind_methods() {
	ClassDB::bind_method(D_METHOD("tick"), &MoverSim::tick);
	ClassDB::bind_method(D_METHOD("get_false"), &MoverSim::get_false);
	ClassDB::bind_method(D_METHOD("set_nRST", "nRST"), &MoverSim::set_nRST);
	ClassDB::bind_method(D_METHOD("set_CLK", "CLK"), &MoverSim::set_CLK);
	ClassDB::bind_method(D_METHOD("get_nWR"), &MoverSim::get_nWR);
	ClassDB::bind_method(D_METHOD("get_nRD"), &MoverSim::get_nRD);
	ClassDB::bind_method(D_METHOD("get_nS0"), &MoverSim::get_nS0);
	ClassDB::bind_method(D_METHOD("set_D", "D"), &MoverSim::set_D);
	ClassDB::bind_method(D_METHOD("get_D"), &MoverSim::get_D);
	ClassDB::bind_method(D_METHOD("get_A"), &MoverSim::get_A);
	ClassDB::bind_method(D_METHOD("get_iPC"), &MoverSim::get_iPC);
	ClassDB::bind_method(D_METHOD("get_iD"), &MoverSim::get_iD);
	ClassDB::bind_method(D_METHOD("get_iS"), &MoverSim::get_iS);
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nRST"), "set_nRST", "get_false"); // input  pin
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "CLK"), "set_CLK", "get_false");   // input  pin
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nWR"), "", "get_nWR");            // output pin
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nRD"), "", "get_nRD");            // output pin
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nS0"), "", "get_nS0");            // output pin
	ADD_PROPERTY(PropertyInfo(Variant::INT, "D"), "set_D", "get_D");            // inout  bus
	ADD_PROPERTY(PropertyInfo(Variant::INT, "A"), "", "get_A");                 // output bus
	ADD_PROPERTY(PropertyInfo(Variant::INT, "iPC"), "", "get_iPC");             // internal PC
	ADD_PROPERTY(PropertyInfo(Variant::INT, "iD"), "", "get_iD");               // internal Data
	ADD_PROPERTY(PropertyInfo(Variant::INT, "iS"), "", "get_iS");               // internal Step
}

MoverSim::MoverSim() {
	ctx = std::make_unique<VerilatedContext>();
	cpu = std::make_unique<Vmain>(ctx.get(), "TOP");
}

MoverSim::~MoverSim() { cpu->final(); }

uint64_t MoverSim::tick() {
	cpu->eval();
	ctx->timeInc(1);
	return ctx->time();
}

bool MoverSim::get_false() const { return false; }
void MoverSim::set_nRST(const bool &b) { cpu->nRST = b; }
void MoverSim::set_CLK(const bool &b) { cpu->CLK = b; }
bool MoverSim::get_nWR() const { return cpu->nWR; }
bool MoverSim::get_nRD() const { return cpu->nRD; }
bool MoverSim::get_nS0() const { return cpu->nS0; }
void MoverSim::set_D(const int &i) { cpu->Di = i & 0xFFFF; }
int MoverSim::get_D() const { return cpu->Do & 0xFFFF; }
int MoverSim::get_A() const { return cpu->A & 0xFFFF; }
int MoverSim::get_iPC() const { return cpu->rootp->Mover__DOT__core__DOT__pc__DOT__val & 0xFFFF; }
int MoverSim::get_iD() const {
	return cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__7__KET____DOT__val << 15
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__6__KET____DOT__val << 14
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__5__KET____DOT__val << 13
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__4__KET____DOT__val << 12
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__3__KET____DOT__val << 11
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__2__KET____DOT__val << 10
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__1__KET____DOT__val << 9
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__1__KET____DOT___dl__BRA__0__KET____DOT__val << 8
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__7__KET____DOT__val << 7
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__6__KET____DOT__val << 6
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__5__KET____DOT__val << 5
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__4__KET____DOT__val << 4
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__3__KET____DOT__val << 3
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__2__KET____DOT__val << 2
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__1__KET____DOT__val << 1
	     | cpu->rootp->Mover__DOT__core__DOT__r_data__DOT___dl__BRA__0__KET____DOT___dl__BRA__0__KET____DOT__val;
}
int MoverSim::get_iS() const {
	return cpu->rootp->Mover__DOT__core__DOT__step__DOT__step_counter_decoder__DOT___counter__DOT__val & 0b111;
}
