#pragma once

#include "../obj_dir/Vmain.h"

#include <godot_cpp/classes/global_constants.hpp>
#include <godot_cpp/classes/ref_counted.hpp>
#include <godot_cpp/core/binder_common.hpp>
#include <memory>
#include <verilated.h>

using namespace godot;

class MoverSim : public RefCounted {
		GDCLASS(MoverSim, RefCounted);

	private:
		std::unique_ptr<Vmain> cpu;
		std::unique_ptr<VerilatedContext> ctx;

	protected:
		static void _bind_methods();

	public:
		MoverSim();
		~MoverSim();
		uint64_t tick();

		bool get_false() const;
		void set_nRST(const bool &b);
		void set_CLK(const bool &b);
		bool get_nWR() const;
		bool get_nRD() const;
		bool get_nS0() const;
		void set_D(const int &i);
		int get_D() const;
		int get_A() const;
		int get_iPC() const;
		int get_iD() const;
		int get_iS() const;
};
