# MoverSim

## playing the game

To just run the game, either run `MoverSim.x86_64` from the `Export` directory in your cloned repository, or from the extracted [Download](/Export/MoverSim.tar.xz).

To run and develop the game or try custom versions, see the installation steps below.


## install for development

(every block of commands starts off in the project root directory)

There are multiple options, pick the first 2 below if you want to edit any C++ parts of the GDExtension or build for a custom `godot` / `godot-cpp` version, pick the last one if you just want to edit the game.

### optimized godot build (optional)

```sh
git submodule update --init --recursive godot
cd godot
touch .gdignore
scons -j6 platform=linuxbsd use_llvm=yes use_lld=yes tools=no target=template_release production=yes optimize=speed lto=none deprecated=no debug_symbols=no build_profile=../engine.build
strip bin/godot.linuxbsd.template_release.x86_64.llvm
cp bin/godot.linuxbsd.template_release.x86_64.llvm ../bin/x11/engine.x86_64
```

### for C++ development (optional)

(ignore this part if you only care about development of the Game in godot)


Build `godot-cpp` first:

```sh
git submodule update --init --recursive godot-cpp
cd godot-cpp
mkdir bin build-release
cd build-release
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release ..
cmake --build . -j 6
cd ../bin
mv ../build-release/bin/* .
ranlib *
```

Build the extension library:

```sh
mkdir build
cd build
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release ..
cmake --build . -j 6
```

run the last (two, after editing `CMakeLists.txt`) command(s) again to rebuild any changes

### for godot development (alternative)

(ignore this part if you've already followed the instructions above)

Copy the extension library from the release:

```sh
mkdir -p bin/x11
cp Export/libmoversim.so bin/x11/
cp Export/MoverSim.x86_64 bin/x11/engine.x86_64
```

## making levels

* inherit new scene from `res://_base.tscn`
* copy script contents from base into new internal script
* edit script contents, memory, clockrate, ...
