extends "res://Levels/_base.gd"

var textTween: Tween
func text(str:String,time:=5.0)->Tween:
	%Message.visible_ratio=0
	%Message.text = str
	if textTween:
		textTween.kill()
	textTween = get_tree().create_tween()
	textTween.tween_property(%Message,'visible_ratio',1,time)
	return textTween

var stage := 0

func _on_ticked()->void:
	super()
	# We want the user to execute "1234 0000", which is a "jump to the address pointed at in 1234"
	# depending on stage, we want to emulate different behavior:
	match stage:
		0: # show initial message
			text("[i]Hello, World[/i]!                                             Welcome to [b]MoverSim[/b], the puzzle game all about moving [b]16 bits[/b] around.

What you see here is an interface to the [i]Mover[/i] [b]Core[/b] via the Bus [b]Monitor[/b] (top left) and [b]Memory[/b] (top) modules.

In this [b]Message[/b] Box, you will get various tasks to fulfill.

For now, your [b]Memory[/b] is limited to 2 Addresses bound at offset 0000 and 0001, both containing the Value 0000.
Try editing the values above. Also, you can bring the [b]Core[/b] into or out of [b]Reset[/b] by toggling [u]!RST[/u] in the [b]Monitor[/b].

To advance, bring the [b]Core[/b] into [b]Reset[/b], set the [b]Memory[/b]'s content to [u]1234 0000[/u], and then execute it by bringing the [b]Core[/b] out of [b]Reset[/b] again.

...",15.0)
			stage = 1
		1: # respond to 1234 with DEAD (essentially fake memory read)
			if SBus.A==0x1234 and !SBus.nRD:
				SBus.D=0xDEAD
				SBus.changed.emit()
				text("...
looking good
...",1.0)
				stage = 2
		2: # respond to execution of DEAD with reset
			if SBus.A==0xDEAD and !SBus.nS0:
				SBus.nRST=false
				SBus.changed.emit()
				text("[b]Congratulations![/b]
[b]Mover[/b] executes commands in pairs of 2 Addresses: the first one is an address to read from, the second one is to write to. This is [i]moving[/i] a value.
This is all it ever does, over and over, except: if the destination (\"write to\", second, ...) address is [u]0000[/u], it instead writes to its internal Program Counter, effectively [i]jumping[/i] to the value.

This test was rigged so that reading from [u]1234[/u] would return a secret value that, when [i]jumped[/i] to by [i]moving[/i] it to [u]0000[/u], passed the test.

... simply [url=nextLevel]click here[/url] to advance to the next level ...
",10.0)
				%Message.meta_clicked.connect(func(meta)->void:
					if meta == "nextLevel":
						_next_level()
				)
				# wait in invalid stage
				stage = -1

func _next_level()->void:
	get_tree().change_scene_to_file("res://playground.tscn")
